//1. Create a readingListActD folder. Inside create an index.html and index.js files for the first part of the activity and create a crud.js file for the second part of the activity.
//2. Once done with your solution, create a repo named 'readingListActD' and push your documents.
//3. Save the repo link on S32-C1

//Part 1:
/*
Create a readingListActD folder. Inside, create an index.html and index.js file. Test the connection of your js file to the html file by printing 'Hello World' in the console.
1.)
Create a student class sectioning system based on their entrance exam score.
If the student average is from 80 and below. Message: Your section is Grade 10 Section Ruby,
If the student average is from 81-120. Message: Your section is Grade 10 Section Opal,
If the student average is from 121-160. Message: Your section is Grade 10 Section Sapphire,
If the student average is from 161-200 to. Message: Your section is Grade 10 Section Diamond

Sample output in the console: Your score is (score). You will become proceed to Grade 10 (section)
*/
// console.log('Hello World')

function getAverage(...numbers) {
  let x = 1;
  let sum = 0;

  for (const number of numbers) {
    sum += number;
    x++
  }

  let average = Math.round(sum / (x - 1));

  if (average <= 80) {
    console.log('Your score is ' + average + '.' + ' You will proceed to Grade 10 Section Ruby')
  } else if (average <=120) {
    console.log('Your score is ' + average + '.' + ' You will proceed to Grade 10 Section Opal')
  } else if (average <= 160) {
    console.log('Your score is ' + average + '.' + ' You will proceed to Grade 10 Section Sapphire')
  } else if (average <=200) {
    console.log('Your score is ' + average + '.' + ' You will proceed to Grade 10 Section Diamond')
  }
}

getAverage(120, 112, 113, 111);
/*
2.) 
Write a JavaScript function that accepts a string as a parameter and find the longest word within the string.

Sample Data and output:
Example string: 'Web Development Tutorial'
Expected Output: 'Development'
*/
function showLongestWord(string){
  let stringArray = string.split(" ").sort();
  console.log(stringArray[0]);
}
showLongestWord('Web Development Tutorial');

/*
3.)
Write a JavaScript function to find the first not repeated character.

Sample arguments : 'abacddbec'
Expected output : 'e'
*/
function getFirstUniqueCharacter(string) {
for (var x = 0; x < string.length; x++) {
    let uniqueCharacter = string.charAt(x);
    if (string.indexOf(uniqueCharacter) == x && string.indexOf(uniqueCharacter, x + 1) == -1) {
      console.log(uniqueCharacter);
      return
    }
  }
  console.log('there was no unique character found');
}

getFirstUniqueCharacter('abacddbec');



